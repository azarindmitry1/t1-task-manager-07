package ru.t1.azarin.tm.model;

import ru.t1.azarin.tm.constant.ArgumentConst;

import ru.t1.azarin.tm.constant.TerminalConst;

public class Command {

    public static Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION,
            "Display application version."
    );

    public static Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT,
            "Display developer info."
    );

    public static Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP,
            "Display application command."
    );

    public static Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO,
            "Display process and memory info."
    );

    public static Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null,
            "Close application."
    );

    private String name = "";

    private String description = "";

    private String argument;

    public Command() {
    }

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";

        if (name != null && !name.isEmpty()) result += name + ", ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;

        return result;
    }

}
