package ru.t1.azarin.tm;

import ru.t1.azarin.tm.constant.ArgumentConst;

import ru.t1.azarin.tm.constant.TerminalConst;

import ru.t1.azarin.tm.model.Command;

import ru.t1.azarin.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        if (processArguments(args)) {
            exit();
            return;
        }

        System.out.println("** WELCOME TO TASK-MANAGER **");
        final Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("ENTER THE COMMAND:");
            String command = scanner.nextLine();
            processCommands(command);
        }
    }

    private static boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;

        final String param = args[0];

        switch (param) {
            case ArgumentConst.ARG_VERSION:
                showVersion();
                break;
            case ArgumentConst.ARG_ABOUT:
                showAbout();
                break;
            case ArgumentConst.ARG_HELP:
                showHelp();
                break;
            case ArgumentConst.ARG_INFO:
                showInfo();
                break;
            default:
                showErrorArg();
        }

        return true;
    }

    private static void processCommands(final String command) {
        if (command == null || command.isEmpty()) {
            showErrorCmd();
            return;
        }

        switch (command) {
            case TerminalConst.CMD_VERSION:
                showVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                showAbout();
                break;
            case TerminalConst.CMD_HELP:
                showHelp();
                break;
            case TerminalConst.CMD_INFO:
                showInfo();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
            default:
                showErrorCmd();
        }
    }

    public static void showErrorCmd() {
        System.out.println("Wrong command. Try again with another command.");
    }

    public static void showErrorArg() {
        System.out.println("Wrong argument. Try again with another argument.");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Dmitry Azarin");
        System.out.println("azarindmitry1@gmail.com");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(Command.VERSION);
        System.out.println(Command.ABOUT);
        System.out.println(Command.HELP);
        System.out.println(Command.INFO);
        System.out.println(Command.EXIT);
    }

    public static void showInfo() {
        System.out.println("[INFO]");

        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available: " + NumberUtil.formatBytes(totalMemory));

        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

    public static void exit() {
        System.exit(0);
    }

}
